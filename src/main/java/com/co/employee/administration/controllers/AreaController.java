package com.co.employee.administration.controllers;

import com.co.employee.administration.entities.Area;
import com.co.employee.administration.entities.Employee;
import com.co.employee.administration.entities.TypeIdentification;
import com.co.employee.administration.exceptions.InternalExceptionError;
import com.co.employee.administration.services.AreaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/area/")
public class AreaController {
    @Autowired
    private AreaService areaService;

    @ApiOperation(value = "View information of areas"
            ,notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. The resource is obtained correctly", response = Employee.class ),
            @ApiResponse(code = 400, message = "Bad Request. There are errors in the information", response = String.class),
            @ApiResponse(code = 500, message = "Unexpected system error") })
    @GetMapping("getAll")
    public List<Area> getAreas() throws InternalExceptionError {
        return areaService.getAreas();
    }
}
