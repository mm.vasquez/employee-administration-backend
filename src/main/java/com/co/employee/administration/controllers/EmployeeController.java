package com.co.employee.administration.controllers;

import com.co.employee.administration.entities.Employee;
import com.co.employee.administration.exceptions.InternalExceptionError;
import com.co.employee.administration.services.EmployeeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/employee/")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @ApiOperation(value = "Employee registration and update"
            ,notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. The resource is obtained correctly", response = Employee.class ),
            @ApiResponse(code = 400, message = "Bad Request. There are errors in the information", response = String.class),
            @ApiResponse(code = 500, message = "Unexpected system error") })
    @PostMapping("registerEmployee")
    public Employee registerEmployee(@RequestBody Employee employee) throws InternalExceptionError {
        return employeeService.createEmployee(employee);
    }

    @ApiOperation(value = "View information of employees"
            ,notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. The resource is obtained correctly", response = Employee.class ),
            @ApiResponse(code = 400, message = "Bad Request. There are errors in the information", response = String.class),
            @ApiResponse(code = 500, message = "Unexpected system error") })
    @GetMapping("getAll")
    public List<Employee> getEmployees() throws InternalExceptionError {
        return employeeService.getEmployees();
    }

    @ApiOperation(value = "Delete employee"
            ,notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. The resource is obtained correctly", response = Employee.class ),
            @ApiResponse(code = 400, message = "Bad Request. There are errors in the information", response = String.class),
            @ApiResponse(code = 500, message = "Unexpected system error") })
    @DeleteMapping("delete/{id}")
    public void deleteEmployee(@PathVariable Integer id){
        employeeService.deleteEmployee(id);
    }

}
