package com.co.employee.administration.services;

import com.co.employee.administration.entities.TypeIdentification;
import com.co.employee.administration.exceptions.InternalExceptionError;
import com.co.employee.administration.repositories.TypeIdentificationRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;

import static org.apache.log4j.helpers.UtilLoggingLevel.SEVERE;

@Service
public class TypeIdentificationService {
    @Autowired
    private TypeIdentificationRepository typeIdentificationRepository;

    private Logger log = Logger.getLogger(TypeIdentificationService.class);

    public List<TypeIdentification> getTypesIdentification() throws InternalExceptionError {
        try {
            return  typeIdentificationRepository.findAll();
        }catch (Exception e){
            log.error(SEVERE, e);
            throw new InternalExceptionError("500", "Internal server error");
        }
    }
}
