package com.co.employee.administration.services;

import com.co.employee.administration.entities.Employee;
import com.co.employee.administration.exceptions.InternalExceptionError;
import com.co.employee.administration.repositories.EmployeeRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.co.employee.administration.util.Constants;

import java.util.List;

import static org.apache.log4j.helpers.UtilLoggingLevel.SEVERE;
import static org.apache.log4j.helpers.UtilLoggingLevel.OFF;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    private Logger log = Logger.getLogger(EmployeeService.class);

    public Employee createEmployee(Employee employee) throws InternalExceptionError {
        Employee employeeExist = null;
        if (employee.getId() == null) {
            employeeExist = employeeRepository.getExistEmployee(employee.getNumberIdentification(), employee.getTypeIdentification().getId());
        }
        if (employeeExist == null){
            String email = employee.getFirstName().toLowerCase().replace(" ", "")+"."+employee.getFirstSurname().toLowerCase().replace(" ", "");
            String domain = (Constants.COUNTRY_COLOMBIA.equals(employee.getCountry())?Constants.DOMAIN_CO:Constants.DOMAIN_US);
            if(employee.getId() == null){
                List<Employee> emailExist = employeeRepository.findByEmail(email, domain);
                if (emailExist.size() > 0) {
                    email = employee.getFirstName().toLowerCase().replace(" ", "")+"."+employee.getFirstSurname().toLowerCase().replace(" ", "")+"."+emailExist.size();
                }
            }
            email += domain;
            employee.setEmail(email);
            log.info("Register employee");
            return employeeRepository.save(employee);
        }else{
            throw new InternalExceptionError("400", "BAD_REQUEST");
        }
    }

    public List<Employee> getEmployees() throws InternalExceptionError {
        try {
            return  employeeRepository.findAll();
        }catch (Exception e){
            log.error(SEVERE, e);
            throw new InternalExceptionError("500", "Internal server error");
        }
    }

    public void deleteEmployee(Integer id){
        try{
            Employee employee = employeeRepository.findById(id).get();
            log.log(OFF, "Delete succes");
            employeeRepository.delete(employee);
        }catch (Exception e){
            log.error(SEVERE, e);
        }
    }

}
