package com.co.employee.administration.services;

import com.co.employee.administration.entities.Area;
import com.co.employee.administration.exceptions.InternalExceptionError;
import com.co.employee.administration.repositories.AreaRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;

import static org.apache.log4j.helpers.UtilLoggingLevel.SEVERE;

@Service
public class AreaService {
    @Autowired
    private AreaRepository areaRepository;

    private Logger log = Logger.getLogger(AreaService.class);

    public List<Area> getAreas() throws InternalExceptionError {
        try {
            return  areaRepository.findAll();
        }catch (Exception e){
            log.error(SEVERE, e);
            throw new InternalExceptionError("500", "Internal server error");
        }
    }
}
