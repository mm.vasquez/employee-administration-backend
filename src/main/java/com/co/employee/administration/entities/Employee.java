package com.co.employee.administration.entities;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = "employee")
public class Employee {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "first_surname", length = 20)
    @NotNull
    private String firstSurname;
    @Column(name = "second_surname", length = 20)
    @NotNull
    private String secondSurname;
    @Column(name = "first_name", length = 20)
    @NotNull
    private String firstName;
    @Column(name = "other_name", length = 50)
    private String otherName;
    @Column(name = "country")
    @NotNull
    private String country;
    @JoinColumn(name = "type_identification_id", referencedColumnName = "id")
    @OneToOne
    @NotNull
    private TypeIdentification typeIdentification;
    @Column(name = "number_identification", length = 20)
    @NotNull
    private String numberIdentification;
    @Column(name = "email", length = 300)
    @NotNull
    private String email;
    @Column(name = "admission_date")
    @NotNull
    private Date admissionDate;
    @Column(name = "status")
    @NotNull
    private Boolean status;
    @Column(name = "create_date")
    private Date createDate;
    @Column(name = "edition_date")
    @UpdateTimestamp
    private Date editionDate;
    @JoinColumn(name = "area_id", referencedColumnName = "id")
    @OneToOne
    private Area areaId;
}
