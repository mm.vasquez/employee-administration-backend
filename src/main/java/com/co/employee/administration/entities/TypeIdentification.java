package com.co.employee.administration.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "type_identification")
public class TypeIdentification {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "description")
    private String description;
}
