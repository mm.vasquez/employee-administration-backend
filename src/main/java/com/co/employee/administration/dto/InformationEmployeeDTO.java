package com.co.employee.administration.dto;

import lombok.Data;

@Data
public class InformationEmployeeDTO {
    private Integer id;
    private String firstName;
    private String otherName;
    private String firstSurname;
    private String secondSurname;
    private String typeIdentification;
    private String numberIdentification;
    private String country;
    private Boolean status;
}
