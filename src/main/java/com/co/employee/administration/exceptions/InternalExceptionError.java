package com.co.employee.administration.exceptions;

import com.co.employee.administration.dto.ErrorDto;
import org.springframework.http.HttpStatus;

import java.util.Arrays;


public class InternalExceptionError extends EmployeeException {
    private static final long serialVersionUID = 1L;

    public InternalExceptionError(String code, String message) {
        super(code, HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

    public InternalExceptionError(String code, String message, ErrorDto data ) {
        super(code, HttpStatus.INTERNAL_SERVER_ERROR.value(), message, Arrays.asList(data));
    }
}
