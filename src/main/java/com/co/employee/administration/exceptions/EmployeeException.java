package com.co.employee.administration.exceptions;

import com.co.employee.administration.dto.ErrorDto;

import java.util.ArrayList;
import java.util.List;

public class EmployeeException extends Exception{

    private static final long serialVersionUID = 1L;
    private final String code;
    private final int responsecode;
    private final List<ErrorDto> errorDtoList = new ArrayList<>();

    public EmployeeException(String code, int responsecode, String message) {
        super(message);
        this.code = code;
        this.responsecode = responsecode;
    }

    public EmployeeException(String code, int responsecode, String message, List<ErrorDto> errorDtoList ) {
        super(message);
        this.code = code;
        this.responsecode = responsecode;
        this.errorDtoList.addAll(errorDtoList);
    }


    public String getCode() {
        return code;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public List<ErrorDto> getErrorDtoList() {
        return errorDtoList;
    }
}
