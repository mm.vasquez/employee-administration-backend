package com.co.employee.administration.repositories;

import com.co.employee.administration.entities.Area;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaRepository extends JpaRepository<Area, Integer> {
}
