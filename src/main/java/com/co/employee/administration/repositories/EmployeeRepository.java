package com.co.employee.administration.repositories;

import com.co.employee.administration.dto.InformationEmployeeDTO;
import com.co.employee.administration.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>  {

    @Query(value="SELECT * FROM employee WHERE number_identification = ?1 AND type_identification_id = ?2",nativeQuery = true)
    Employee getExistEmployee(String numberIdentification, Integer typeIdentification);

    @Query(value="SELECT * FROM employee WHERE email LIKE %?1% AND email LIKE %?2%", nativeQuery = true)
    List<Employee> findByEmail(String name, String domain);


}
