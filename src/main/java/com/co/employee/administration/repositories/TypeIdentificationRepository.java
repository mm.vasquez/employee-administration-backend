package com.co.employee.administration.repositories;

import com.co.employee.administration.entities.TypeIdentification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeIdentificationRepository extends JpaRepository<TypeIdentification, Integer> {
}
